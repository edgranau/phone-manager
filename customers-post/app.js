var AWS = require('aws-sdk');
var uuid = require('uuid/v1');
const awsRegion = process.env.AWS_REGION || 'ap-southeast-2';
const options = Object.assign(
  {},
  // { region: awsRegion },
  process.env.AWS_SAM_LOCAL ? { endpoint: 'http://dynamodb:8000' } : {}
);
console.log(process.env.CUSTOMERS_TABLE);
var docClient = new AWS.DynamoDB.DocumentClient(options);

exports.handler = async (event) => {
  console.log(JSON.stringify(event));
  var name = JSON.parse(event.body).name;
  var headers = {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
  };
  return docClient
    .put({
      TableName: process.env.CUSTOMERS_TABLE,
      Item: {
        id: uuid(),
        name: name,
      },
    })
    .promise()
    .then((result) => {
      console.log(JSON.stringify(result));
      return { headers: headers, statusCode: 200, body: JSON.stringify(result) };
    })
    .catch((err) => {
      console.log(JSON.stringify(err));
      return { headers: headers, statusCode: 500, body: JSON.stringify(err) };
    });
};
