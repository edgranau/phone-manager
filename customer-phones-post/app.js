var AWS = require('aws-sdk');
const awsRegion = process.env.AWS_REGION || 'ap-southeast-2';
const options = Object.assign(
  {},
  { region: awsRegion },
  process.env.AWS_SAM_LOCAL ? { endpoint: 'http://dynamodb:8000' } : {}
);
var docClient = new AWS.DynamoDB.DocumentClient(options);

exports.handler = async (event) => {
  console.log(JSON.stringify(event));
  var customerId = event.pathParameters ? event.pathParameters['customerId'] || null : null;
  var phone = event.pathParameters ? event.pathParameters['phone'] || null : null;
  var headers = {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
  };

  const phoneIsActive = await isActive(phone);
  if (phoneIsActive) {
    try {
      await activatePhone(phone);
      await addPhoneToCustomer(phone, customerId);
      return { headers: headers, statusCode: 200, body: JSON.stringify({ customerId: customerId, phone: phone }) };
    } catch (err) {
      return { headers: headers, statusCode: 500, body: JSON.stringify(err) };
    }
  }
  return {
    headers: headers,
    statusCode: 500,
    body: JSON.stringify(`Phone number ${phone} is already active with another customer.`),
  };
};

const isActive = async (phone) => {
  return docClient
    .query({
      TableName: process.env.CUSTOMER_PHONES_TABLE,
      IndexName: process.env.CUSTOMER_PHONES_INDEX,
      KeyConditionExpression: 'phone = :phone',
      ExpressionAttributeValues: {
        ':phone': phone,
      },
    })
    .promise()
    .then((result) => {
      console.log(JSON.stringify(result));
      return result['Count'] === 0;
    })
    .catch((err) => {
      console.log(JSON.stringify(err));
      throw err;
    });
};

const activatePhone = async (phone) => {
  return docClient
    .put({
      TableName: process.env.PHONES_TABLE,
      Item: {
        phone: phone,
        active: 'true',
      },
    })
    .promise()
    .then((result) => {
      console.log(JSON.stringify(result));
      return true;
    })
    .catch((err) => {
      console.log(JSON.stringify(err));
      throw err;
    });
};

const addPhoneToCustomer = async (phone, customerId) => {
  return docClient
    .put({
      TableName: process.env.CUSTOMER_PHONES_TABLE,
      Item: {
        customerId: customerId,
        phone: phone,
      },
    })
    .promise()
    .then((result) => {
      console.log(JSON.stringify(result));
      return true;
    })
    .catch((err) => {
      console.log(JSON.stringify(err));
      throw err;
    });
};
