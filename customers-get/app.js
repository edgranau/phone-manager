var AWS = require('aws-sdk');
const awsRegion = process.env.AWS_REGION || 'ap-southeast-2';
const options = Object.assign(
  {},
  { region: awsRegion },
  process.env.AWS_SAM_LOCAL ? { endpoint: 'http://dynamodb:8000' } : {}
);
var docClient = new AWS.DynamoDB.DocumentClient(options);

exports.handler = async (event) => {
  console.log(JSON.stringify(event));

  var headers = {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
  };
  var responsePromise = docClient.scan({ TableName: process.env.CUSTOMERS_TABLE }).promise();

  return responsePromise
    .then((result) => {
      console.log(JSON.stringify(result));
      return { headers: headers, statusCode: 200, body: JSON.stringify(result['Items']) };
    })
    .catch((err) => {
      console.log(JSON.stringify(err));
      return { headers: headers, statusCode: 500, body: JSON.stringify(err) };
    });
};
