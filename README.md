# phone-manager

> Coding test for Belong

## Background

We are a Telecom operator. In our database, we are starting to store phone numbers associated to customers (1 customer: N phone numbers) and we will provide interfaces to manage them.
We need to provide the below capabilities:

- get all phone numbers
- get all phone numbers of a single customer
- activate a phone number

## Challenge

Provide interface specifications for the above functions / capabilities.
Provide implementation of the formulated specifications.
You can assume the phone numbers as a static data structure that is initialised when your program runs.

## Language

Java / Lambda (NodeJS, Python or Java only)

## Deliverables

Interface specifications, the source files and any test code – preferably in a github / bitbucket public repo.

The solution will be evaluated based on the following criteria:

- Correctness
- Code structure
- Data structures
- Extensibility
- Maintainability
- Test coverage
- Performance

## Implementation Notes

### Assumptions

- Phone number list is static and it's loadesd into the system before running any services.
- Customer list starts empty.
- Customers can be added to the system.
- A customer can have multiple phone numbers. This information is stored in a separate entity.

### Data Model

There are three entities in the data model. These models contain the bare minimun attributes necessary to fulfill the requirements of the challenge.

| Entity         | key               | Attributes           |
| -------------- | ----------------- | -------------------- |
| Phones         | phone             | active (True\|False) |
| Customers      | id                | name                 |
| CustomerPhones | customerId, phone |                      |

### API Design

There are two endpoints (`/phones`, `/customers`) and five operations (lambda functions) that implement the capabilities required:

- `/phones [GET]`
  - Gets all the phone numbers (R1)
- `/customers/{customerId}/phones [GET]`
  - Get all phone numbers of a single customer (R2)
- `/customers/{customerId}/phones/{phone} [POST]`
  - Associates (Activates) a phone number to an existing customer (R3)
    - phone number cannot be associated to another customer
- `/customers [GET]`
  - Get all customers on the system
- `/customers [POST]`
  - Creates a new customer in the system
    - it does *not* associate this new customer with a phone number
    - expected body payload:

      ```json
      {
        "name": "NAME"
      }
      ```

### Technology Decisions

To implement this solution I'm going to use [AWS SAM](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/what-is-sam.html). This will help me set up the infrastructure required and iterate quickly over the development of all the lambda functions required to fulfill the specification

### Requirements

These are the tools required in my MacOS dev environment:

```shell
# Docker
brew cask install docker

# Node
brew install nvm
nvm install 12 --lts
nvm use 12

# AWS CLI and SAM CLI
brew install awscli
brew tap aws/tap
brew install aws-sam-cli

# DynamoDB local
docker pull amazon/dynamodb-local
```

### Local Development (Getting Started)

- Set up Local DynamoDB

```shell
aws configure --profile dynamo-local
# dynamodb-local needs credential, even if it is offline.
# It's not necesary to add real AWS credentials. You can use fake key and secret in this profile
docker network create lambda-local
docker run --network lambda-local -h dynamodb -p 8000:8000 --name dynamodb amazon/dynamodb-local -Djava.library.path=./DynamoDBLocal_lib -jar DynamoDBLocal.jar -sharedDb
aws dynamodb create-table --cli-input-yaml file://dynamo-local/customer-phones-table.yaml --endpoint-url http://localhost:8000 --profile dynamo-local
aws dynamodb create-table --cli-input-yaml file://dynamo-local/customers-table.yaml --endpoint-url http://localhost:8000 --profile dynamo-local
aws dynamodb create-table --cli-input-yaml file://dynamo-local/phones-table.yaml --endpoint-url http://localhost:8000 --profile dynamo-local
```

- Populate PhonesTable

```shell
aws dynamodb batch-write-item --cli-input-yaml file://dynamo-local/phones-data.yaml --endpoint-url http://localhost:8000 --profile dynamo-local
```

- Build project locally

```shell
sam build --use-container
```

- Test lambdas locally

```shell
sam local invoke GetCustomerPhonesFunction --docker-network lambda-local --event events/get-customer-phones-event.json
sam local invoke GetCustomersFunction --docker-network lambda-local --event events/get-customers-event.json
sam local invoke GetPhonesFunction --docker-network lambda-local --event events/get-phones-event.json
sam local invoke PostCustomerPhonesFunction --docker-network lambda-local --event events/post-customer-phones-event.json
sam local invoke PostCustomersFunction --docker-network lambda-local --event events/post-customers-event.json
```

- Start the API

```shell
sam local start-api --docker-network lambda-local
# Mounting GetPhonesFunction at http://127.0.0.1:3000/phones [GET]
# Mounting GetCustomerPhonesFunction at http://127.0.0.1:3000/customers/{customerId}/phones [GET]
# Mounting PostCustomerPhonesFunction at http://127.0.0.1:3000/customers/{customerId}/phones/{phone} [POST]
# Mounting GetCustomersFunction at http://127.0.0.1:3000/customers [GET]
# Mounting PostCustomersFunction at http://127.0.0.1:3000/customers [POST]
# You can now browse to the above endpoints to invoke your functions. You do not need to restart/reload SAM CLI while working on your functions, changes will be reflected instantly/automatically. You only need to restart SAM CLI if you update your AWS SAM template
# 2020-03-21 15:47:24  * Running on http://127.0.0.1:3000/ (Press CTRL+C to quit)
```

- Test the APIs

```shell
curl --location --request POST 'http://127.0.0.1:3000/customers' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "Edwin"
}'
```

- Build & Deploy in AWS

```shell
sam build \
  --profile AWS_PROFILE

sam deploy \
  --stack-name sam-phone-manager \
  --capabilities CAPABILITY_IAM \
  --region ap-southeast-2 \
  --profile AWS_PROFILE
```

### Cleanup

```shell
aws cloudformation delete-stack \
  --stack-name qsam-phone-manager \
  --region ap-southeast-2 \
  --profile AWS_PROFILE
```
